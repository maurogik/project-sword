package common;

import java.awt.Point;
import java.io.Serializable;

/**
 * Class holding the data to transfer to the client so it can update its sate of the game
 * @author Gwenn
 *
 */
public class PlayersData implements Serializable{

	private static final long serialVersionUID = -43071508415101810L;
	
	public boolean isGamePaused;
	public int time;
	
	public Point player1location;
	public Point player2location;
	
	public int player1AnimationID;
	public int player2AnimationID;
	
	public int player1AnimationFrame;
	public int player2AnimationFrame;
	
	public boolean player1CurrentAnimIsReversed;
	public boolean player2CurrentAnimIsReversed;
	
	public int player1score;
	public int player2score;
	
	public boolean player1hit;
	public boolean player2hit;
	
	public PlayersData(){
		
		isGamePaused=false;
		
		player1location=null;
		player1AnimationID=-1;
		player1AnimationFrame=-1;
		player1CurrentAnimIsReversed=false;
		player1score=0;
		
		player2location=null;
		player2AnimationID=-1;
		player2AnimationFrame=-1;
		player2CurrentAnimIsReversed=false;
		player2score=0;
		
		player1hit=false;
		player2hit=false;
		
	}
}
