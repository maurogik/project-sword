package common;

import java.util.Random;
/**
 * The class running the AI for a character<br>
 * It only run a fairly simple AI with a move and hit goal, nothing fancy here.
 * @author Gwenn
 *
 */
public class CharacterAI {
	/**The character to manage*/
	private Character player = null;

	/**
	 * Create a new instance of AI manager
	 * @param player the character to manage
	 */
	public CharacterAI(Character player) {
		this.player = player;
	}

	/**
	 * Calculate the next AI move to perform
	 * @param ennemy the ennemy to fight
	 * @return the next animation to perform
	 */
	public int calculateNextMove(Character ennemy) {
		if (Math.abs(ennemy.getSword().getX1() - player.getSword().getX1()) < 250) {
			if (ennemy.getCurrentAnimationIndex() >= Character.STRAIGHT_ATTACK_ANIMATION_INDEX
					&& ennemy.getCurrentAnimationIndex() <= Character.UP_ATTACK_ANIMATION_INDEX) {
				return Character.BACKWARD_ANIMATION_INDEX;
			} else {
				return calculateAttack();
			}
		} else {
			return Character.FORWARD_ANIMATION_INDEX;
		}
	}
	
	/**
	 * Choose a random attack to perform
	 * @return the animation to perform
	 */
	private int calculateAttack() {
		Random rand = new Random();
		int res;
		res = rand.nextInt(3);
		res+=Character.STRAIGHT_ATTACK_ANIMATION_INDEX;
		return res;
	}

}
