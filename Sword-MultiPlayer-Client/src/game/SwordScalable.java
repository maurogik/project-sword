package game;

import java.util.ArrayList;
import gui.AbstractState;
import gui.SwordLauncher;
import common.*;
import common.Character;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import tools.SwordFont;
import tools.SwordFont.FontName;
import tools.SwordFont.FontSize;

/**
 * The Basic version of the sword game<br>
 * Allow two player to figth on the same computer
 * @author Gwenn
 *
 */
public class SwordScalable extends AbstractState {
	

	/**
	 * The Scale enum allow the game to loads diferrent image according to the
	 * chosen scale
	 */
	public static enum Scale {
		Small(0.5f) {
			{
				guardStraightSource = imgPath + "StandToStraight_Small.png";
				guardUpSource = imgPath + "StandToUp_Small.png";
				guardDownSource = imgPath + "StandToDown_Small.png";
				attackStraightSource = imgPath + "StandToAtt_Small.png";
				attackUpSource = imgPath + "StandToAttUp_Small.png";
				attackDownSource = imgPath + "StandToAttDown_Small.png";
				ForwardSource = imgPath + "Forward_Small.png";
			}
		},
		Normal(1f) {
			{
				guardStraightSource = imgPath + "StandToStraight.png";
				guardUpSource = imgPath + "StandToUp.png";
				guardDownSource = imgPath + "StandToDown.png";
				attackStraightSource = imgPath + "StandToAtt.png";
				attackUpSource = imgPath + "StandToAttUp.png";
				attackDownSource = imgPath + "StandToAttDown.png";
				ForwardSource = imgPath + "Forward.png";
			}
		};

		public String guardStraightSource = null;
		public String guardUpSource = null;
		public String guardDownSource = null;
		public String attackStraightSource = null;
		public String attackUpSource = null;
		public String attackDownSource = null;
		public String ForwardSource = null;

		/** The scale value */
		public float value = -1;

		private Scale(float v) {
			value = v;
		}
	}

	/*--------------------sound loading data-----------------*/
	private static String soundPath = "sources/sound/";
	private String hitSoundPath = soundPath + "hit.ogg";
	private Sound hitSound;

	/*------------------Image loading data--------------------*/
	private static String imgPath = "sources/data/";
	private String backgroundPath = imgPath + "game_background.jpg";
	private String lightONPath = imgPath + "lightON.png";
	private String lightOFFPath = imgPath + "lightOFF.png";
	private Image backgroundImg;
	private Image lightONImg;
	private Image lightOFFImg;

	/*-----------------Characters declaration------------------*/

	/** The first player */
	private Character player1 = null;
	/** The second player */
	private Character player2 = null;

	/*-----------------Rendering data-------------------*/

	/** Hight multiplicator used to adapt the graphics to the user's resolution */
	private float heigthCoef = 1;
	/** Width multiplicator used to adapt the graphics to the user's resolution */
	private float widthCoef = 1;
	/** The relative width, used for screen resolution adaptation */
	private float relativeWidth = 1680;
	/** The relative height, used for screen resolution adaptation */
	private float relativeHeight = 1050;

	/*------------Miscelaneous---------------*/

	/** The font used to draw strings in the game */
	private Font gameFont = null;
	/** The scale of the game, Small or Normal */
	private Scale scale = Scale.Normal;
	/**Whether or not the game is paused*/
	private boolean gamePaused=false;
	/**The time the game has been paused for*/
	private float pausedTime=0;

	public static boolean isSimpleGameRunning=false;
	
	/**
	 * Create a new game
	 * 
	 * @param title
	 */
	public SwordScalable() {
		super();
	}

	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {
		
		//rendering data initialization
		int animSize = (int) (600 * scale.value);
		heigthCoef = container.getHeight() / relativeHeight;
		widthCoef = container.getWidth() / relativeWidth;

		// sound loading
		super.initMusic(soundPath + "music.ogg", 0.2f, true);
		hitSound = new Sound(hitSoundPath);
		
		//image loading
		backgroundImg=new Image(backgroundPath);
		lightOFFImg=new Image(lightOFFPath);
		lightONImg=new Image(lightONPath);

		//Animation initialization
		ArrayList<CharacterAnimation> animListPlayer1 = new ArrayList<CharacterAnimation>();
		SpriteSheet sprite = new SpriteSheet(scale.guardStraightSource,
				animSize, animSize);
		CharacterAnimation anim = new CharacterAnimation(sprite, 20);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.guardDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 20);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.guardUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 20);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackStraightSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 60);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 80);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 80);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.ForwardSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 50);
		animListPlayer1.add(anim);

		anim = anim.getReversedAnimation();
		animListPlayer1.add(anim);

		ArrayList<CharacterAnimation> animListPlayer2 = new ArrayList<CharacterAnimation>();

		ArrayList<CharacterAnimation> reversedAnimListPlayer1 = new ArrayList<CharacterAnimation>();
		ArrayList<CharacterAnimation> reversedAnimListPlayer2 = new ArrayList<CharacterAnimation>();

		for (int i = 0; i < animListPlayer1.size(); i++) {
			// Add the reversed animation for player 1
			reversedAnimListPlayer1.add(animListPlayer1.get(i)
					.getReversedAnimation());
			// Add the animation for player2
			animListPlayer2.add(animListPlayer1.get(i).getFlippedAnimation());
			// Add the reversed animation for player2
			reversedAnimListPlayer2.add(animListPlayer2.get(i)
					.getReversedAnimation());
		}

		//Player creation
		player1 = new Character("player 1", new Vector2f(40, 300),
				(int) animSize, animSize, (int) (50), animListPlayer1,
				reversedAnimListPlayer1);
		player1.setColor(Color.green);

		player2 = new Character("player 2", new Vector2f(1000, 305), animSize,
				animSize, (int) (-50), animListPlayer2, reversedAnimListPlayer2);
		player2.setColor(Color.red);

		gameFont = SwordFont.getFont(FontName.Batmfa, FontSize.Medium);
		gamePaused=false;
		pausedTime=0;
		isSimpleGameRunning=true;
	}

	/**
	 * Update the game environment with the user entries.
	 */
	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {

		Input input = gc.getInput();

		if (input.isKeyDown(Input.KEY_ESCAPE)) {
			game.enterState(SwordLauncher.StateList.PAUSESTATE.ID);
		}
		
		if (input.isKeyDown(Input.KEY_P)) {
			unpause();
			resetPlayers();
		} else if (input.isKeyDown(Input.KEY_1)) {
			player1.setAiActivated(!player1.isAiActivated());
		} else if (input.isKeyDown(Input.KEY_2)) {
			player2.setAiActivated(!player2.isAiActivated());
		}
		int animationIndexPlayer1 = -1;
		int animationIndexPlayer2 =  -1;

		if (!gamePaused) {

			pausedTime = 0;

			if (player1.isAiActivated()) {
				animationIndexPlayer1 = player1.nextAImove(player2);
			} else {

				if (input.isKeyDown(Input.KEY_SPACE)
						&& input.isKeyDown(Input.KEY_Z)) {
					animationIndexPlayer1 = Character.UP_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_SPACE)
						&& input.isKeyDown(Input.KEY_S)) {
					animationIndexPlayer1 = Character.DOWN_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_SPACE)) {
					animationIndexPlayer1 = Character.STRAIGHT_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)
						&& input.isKeyDown(Input.KEY_Z)) {
					animationIndexPlayer1 = Character.UP_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)
						&& input.isKeyDown(Input.KEY_S)) {
					animationIndexPlayer1 = Character.DOWN_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)) {
					animationIndexPlayer1 = Character.STRAIGHT_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_D)
						&& player1.getLocation().x
								+ player1.getMouvmentOffset() < player2
								.getLocation().x - 100* widthCoef/scale.value) {
					animationIndexPlayer1 = Character.FORWARD_ANIMATION_INDEX;
				} else if (input.isKeyDown(Input.KEY_Q)
						&& player1.getLocation().x
								- player1.getMouvmentOffset() > 0) {
					animationIndexPlayer1 = Character.BACKWARD_ANIMATION_INDEX;
				}
			}

			animationIndexPlayer2 = -1;

			if (player2.isAiActivated()) {
				animationIndexPlayer2 = player2.nextAImove(player1);
			} else {
				if (input.isKeyDown(Input.KEY_ENTER)
						&& input.isKeyDown(Input.KEY_UP)) {
					animationIndexPlayer2 = Character.UP_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_ENTER)
						&& input.isKeyDown(Input.KEY_DOWN)) {
					animationIndexPlayer2 = Character.DOWN_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_ENTER)) {
					animationIndexPlayer2 = Character.STRAIGHT_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_ADD)
						&& input.isKeyDown(Input.KEY_UP)) {
					animationIndexPlayer2 = Character.UP_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_ADD)
						&& input.isKeyDown(Input.KEY_DOWN)) {
					animationIndexPlayer2 = Character.DOWN_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_ADD)) {
					animationIndexPlayer2 = Character.STRAIGHT_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LEFT)
						&& player2.getLocation().x
								- player2.getMouvmentOffset() > player1
								.getLocation().x + 200* widthCoef /scale.value) {
					animationIndexPlayer2 = Character.FORWARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_RIGHT)
						&& player2.getLocation().x
								+ player2.getBaseImage().getWidth()
								- player2.getMouvmentOffset() < relativeWidth) {
					animationIndexPlayer2 = Character.BACKWARD_ANIMATION_INDEX;
				}
			}

			player1.updatePlayer(animationIndexPlayer1);
			player2.updatePlayer(animationIndexPlayer2);

			//Tests for hit and block
			if (player1.isHitBy(player2) && player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null
					&& player2.getCurrentAnim() != null) {
				pause();
			} else if (player1.isHitBy(player2)
					&& player2.getCurrentAnim() != null) {
				player2.addPoint();
				pause();
			} else if (player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null) {
				player1.addPoint();
				pause();
			} else if (player1.isBlocking(player2.getSword())
					&& player2.isBlocking(player1.getSword())) {
				player1.performReversedAnimation(
						player1.getCurrentAnimationIndex(), true);
				player2.performReversedAnimation(
						player2.getCurrentAnimationIndex(), true);
			} else if (player2.isBlocking(player1.getSword())) {
				player1.performReversedAnimation(
						player1.getCurrentAnimationIndex(), true);
			} else if (player1.isBlocking(player2.getSword())) {
				player2.performReversedAnimation(
						player2.getCurrentAnimationIndex(), true);
			}
		} else {
			pausedTime+=delta;
			if(pausedTime>2000){
				unpause();
				resetPlayers();
			}
		}
	}

	/**
	 * Render the game environment
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		g.setBackground(Color.darkGray);
		backgroundImg.draw(0, 0, container.getWidth(), container.getHeight());
		drawCurrentAnimation(player1, g);
		drawCurrentAnimation(player2, g);
		drawGUI(g);
	}


	/**
	 * Draw the Graphical User Interface (strings and spot lights mostly...)
	 * 
	 * @param g
	 */
	private void drawGUI(Graphics g) {
		int size = 100;
		String message = "";

		Color textColor = Color.white;
		Color neutral=Color.lightGray;
		g.setColor(neutral);

		if (gamePaused) {

			if (player1.isHitBy(player2) && player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null
					&& player2.getCurrentAnim() != null) {
				g.setColor(Color.white);
				g.fillOval(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				lightONImg.draw(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				lightONImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				message = "Draw ! No points awarded.";
				
				if (!hitSound.playing()) {
					hitSound.play(1.0f, 0.8f);
				}
				
			} else if (player1.isHitBy(player2)
					&& player2.getCurrentAnim() != null){
				g.setColor(neutral);
				g.fillOval(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				g.setColor(player2.getColor());
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				lightOFFImg.draw(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				lightONImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				message = "player 1 is hit ! player 2 gain one point.";
				textColor = player2.getColor();
				
				if (!hitSound.playing()) {
					hitSound.play(1.0f, 0.8f);
				}
				
			} else if  (player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null){
				g.setColor(player1.getColor());
				g.fillOval(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				g.setColor(neutral);
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				lightONImg.draw(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
						size * heigthCoef);
				lightOFFImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef, size
						* widthCoef, size * heigthCoef);
				message = "player 2 is hit ! player 1 gain one point.";
				textColor = player1.getColor();
				
				if (!hitSound.playing()) {
					hitSound.play(1.0f, 0.8f);
				}
				
			}
		} else {
			g.fillOval(50 * widthCoef, 50 * heigthCoef, size * widthCoef, size
					* heigthCoef);
			g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, size
					* widthCoef, size * heigthCoef);
			lightOFFImg.draw(50 * widthCoef, 50 * heigthCoef, size * widthCoef,
					size * heigthCoef);
			lightOFFImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef, size
					* widthCoef, size * heigthCoef);
		}

		gameFont.drawString(
				(relativeWidth / 2 - gameFont.getWidth(message) / 2)
						* widthCoef,
				(relativeHeight / 2 - gameFont.getHeight(message) / 2)
						* heigthCoef, message, textColor);

		String sco = "player  1 : " + player1.getScore() + "\nplayer 2 : "
				+ player2.getScore();
		
		gameFont.drawString((relativeWidth / 2 - gameFont.getWidth(sco) / 2)
				* widthCoef, 50 * heigthCoef, sco, Color.yellow);
		

		String aiMes = "";
		if (player1.isAiActivated()) {
			aiMes = "player1 Ai";
			gameFont.drawString(20 * widthCoef, 200 * heigthCoef, aiMes,
					player1.getColor());
		}
		if (player2.isAiActivated()) {
			aiMes = "player2 Ai";
			gameFont.drawString((1680 - gameFont.getWidth(aiMes) - 20)
					* widthCoef, 200 * heigthCoef, aiMes, player2.getColor());
		}
	}

	/**
	 * Draw the current animation of the character
	 */
	private void drawCurrentAnimation(Character player, Graphics g) {

		if (player.getCurrentAnim() != null) {

			player.getCurrentAnim().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef / scale.value,
					player.getHeigth() * heigthCoef / scale.value);

		} else {
			player.getBaseImage().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef / scale.value,
					player.getHeigth() * heigthCoef / scale.value);

		}

	}

	/**
	 * Pause the game and all of it's players
	 */
	private void pause() {
		player1.pause();
		player2.pause();
		gamePaused=true;
	}

	/**
	 * Unpause the game and the players
	 */
	private void unpause() {
		player1.unpause();
		player2.unpause();
		gamePaused=false;
	}

	/**
	 * Resets the animation for the players of the game
	 */
	private void resetPlayers() {
		player1.resetPlayer();
		player2.resetPlayer();
	}

	/**
	 * Get the state ID
	 */
	@Override
	public int getID() {
		return SwordLauncher.StateList.SIMPLEGAME.ID;
	}

	
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.enter(container, game);
	}
}
