package gui;


import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class SwordMenuState extends AbstractMenuState {

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		super.init(arg0, arg1);

		this.setMenuTitle("Welcome");
		this.addMenuItem("Connect to a game", true);
		this.addMenuItem("Host a game", true);
		this.addMenuItem("Simple game", true);
		this.addMenuItem("Help", true);
		this.addMenuItem("Credits", true);
		this.addMenuItem("Quit", true);
	}

	@Override
	public int getID() {
		return SwordLauncher.StateList.MENUSTATE.ID;
	}

	@Override
	protected void indexSelectedEvent(int index, StateBasedGame game) {

		switch (index) {
			case 0 :
				game.enterState(SwordLauncher.StateList.CONNEXIONSTATE.ID);
				break;
			case 1 :
				game.enterState(SwordLauncher.StateList.HOSTSERVER.ID);	
				break;
			case 2:
				game.enterState(SwordLauncher.StateList.SIMPLEGAME.ID);	
				break;
			case 3 :
				game.enterState(SwordLauncher.StateList.HELPSTATE.ID);
				break;
			case 4 :
				game.enterState(SwordLauncher.StateList.CREDITSTATE.ID);	
				break;
			case 5 :
				((SwordLauncher) game).quitGame();
				break;
			default :
				break;
		}

	}

	@Override
	public void enter(GameContainer gc, StateBasedGame game)
			throws SlickException {
		super.enter(gc, game);
	}

}
