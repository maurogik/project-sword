package gui;

import java.net.UnknownHostException;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import tools.SwordFont;

public class HostServerState extends AbstractState{

	private static final String TITLE_TEXT = "Start Sword Server.exe";
	private String hostIP="";
	private Font font, fontTitle;

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		// Call super method
		super.init(gc, game);

		// Fonts
		font = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Medium);
		fontTitle = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Large);
		
		try {
			hostIP=java.net.InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			hostIP="no ip found";
			e.printStackTrace();
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		// Call super method
		super.update(gc, game, delta);
		Input input = gc.getInput();

		// Validation
		if (input.isKeyPressed(Input.KEY_ENTER)) {
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
		}

		// Escape key
		if (input.isKeyPressed(Input.KEY_ESCAPE))
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
	}

	
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		// Call super method
		super.render(gc, game, g);

		// Text field
		g.resetFont();
		g.resetLineWidth();
		g.setColor(Color.white);
		g.setLineWidth(4.0f);

		// Title
		int titleWidth = fontTitle.getWidth(TITLE_TEXT);
		int titleHeight = fontTitle.getHeight(TITLE_TEXT);
		
		int ipWidth = font.getWidth(hostIP);
		int ipHeight = font.getHeight(hostIP);
		
		//String message ="the server address will be : ";
		String message ="Server integration is not available yet";
		hostIP="use the server app";

		int msgWidth = font.getWidth(message);
		int msgHeight = font.getHeight(message);
		
		fontTitle.drawString(gc.getWidth() / 2 - titleWidth / 2, gc.getHeight()
				/ 2 - titleHeight - 64.0f, TITLE_TEXT, Color.yellow);
		font.drawString(gc.getWidth() / 2 - ipWidth / 2, gc.getHeight()
				/ 2 - ipHeight+80.0f, hostIP, Color.red);
		font.drawString(gc.getWidth() / 2 - msgWidth / 2, gc.getHeight()
				/ 2 - msgHeight+20.0f, message, Color.yellow);
		
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame game)
			throws SlickException {
		super.enter(gc, game);
	}
	@Override
	public int getID() {
		return SwordLauncher.StateList.HOSTSERVER.ID;
	}

}
