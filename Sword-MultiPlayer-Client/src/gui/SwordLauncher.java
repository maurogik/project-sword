package gui;

import game.SwordScalable;
import java.awt.Toolkit;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.GameState;
import org.newdawn.slick.state.StateBasedGame;
import client.SwordClientScalable;

/**
 * The SwordLauncher class<br>
 * Run the application
 * @author Gwenn
 *
 */
public class SwordLauncher extends StateBasedGame {

	/**
	 * The enum of the different sates of the game
	 * @author Gwenn
	 *
	 */
	public enum StateList {
		SWORDSTATE(0), MENUSTATE(1), CONNEXIONSTATE(2), PAUSESTATE(3), CONNEXIONERRORSTATE(
				4), HOSTSERVER(5),CREDITSTATE(6),SIMPLEGAME(7),HELPSTATE(8);

		public int ID;
		private StateList(int ind) {
			ID = ind;
		}
	}

	/**The IP of the server*/
	public static String serverIP;

	/**
	 * Create the laucher<br>
	 * Add the states to the game
	 * @param name
	 */
	public SwordLauncher(String name) {
		super(name);

		serverIP="";
		
		this.addState(new SwordClientScalable());
		this.addState(new SwordMenuState());
		this.addState(new ConnexionState());
		this.addState(new PauseMenuState());
		this.addState(new ConnexionErrorState());
		this.addState(new HostServerState());
		this.addState(new CreditState());
		this.addState(new SwordScalable());
		this.addState(new HelpSate());

		this.enterState(StateList.MENUSTATE.ID);
	}

	/**
	 * Initialize all the states
	 */
	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		for (StateList state : StateList.values()) {
			initState(state.ID);
		}
	}

	/**
	 * Initialize a state
	 * @param state
	 */
	public void initState(int state) {
		try {
			GameState bs = getState(state);
			System.out.println(bs.getClass());
			bs.init(this.getContainer(), this);
		} catch (SlickException exc) {
			exc.printStackTrace();
		}
	}

	/**
	 * Quit the game
	 */
	public void quitGame() {
		this.getContainer().setForceExit(false);
		this.getContainer().exit();
		System.exit(0);
	}

	public static void main(String[] args) throws SlickException {
		try {
			AppGameContainer app = new AppGameContainer(new SwordLauncher(
					"Sword"));

			app.setDisplayMode(
					Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit
							.getDefaultToolkit().getScreenSize().height, true);
			 
			app.setTargetFrameRate(120);
			app.setAlwaysRender(true);
			app.setVerbose(false);
			app.setShowFPS(false);
			app.start();
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
