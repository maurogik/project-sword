package gui;


import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.StateBasedGame;

import client.SwordClientScalable;


import tools.SwordFont;

public class ConnexionState extends AbstractState {

	public static final int MAXSIZE = 15;
	private static final int TEXTFIELD_MARGIN = 4;

	private static final String TITLE_TEXT = "Type the server IP";

	private TextField textField;
	private Font font, fontTitle;

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		// Call super method
		super.init(gc, game);

		// Fonts
		font = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Medium);
		fontTitle = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Large);

		// Text field
		int tfWidth = font.getWidth("W") * (MAXSIZE + 2) + 2
				* TEXTFIELD_MARGIN;
		int tfHeight = font.getHeight("Azerty001") + 2 * TEXTFIELD_MARGIN;

		textField = new TextField(gc, font, gc.getWidth() / 2 - tfWidth / 2,
				gc.getHeight() / 2 - tfHeight / 2, tfWidth, tfHeight);
		textField.setBackgroundColor(new Color(1.0f, 1.0f, 0.0f, 0.1f));
		textField.setBorderColor(Color.yellow);
		textField.setCursorVisible(true);
		textField.setMaxLength(MAXSIZE);
		textField.setTextColor(Color.white);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		// Call super method
		super.update(gc, game, delta);

		Input input = gc.getInput();

		// Validation
		if (input.isKeyPressed(Input.KEY_ENTER)) {
			
			// Load game
			SwordLauncher.serverIP=textField.getText();
			SwordClientScalable cli=(SwordClientScalable)game.getState(SwordLauncher.StateList.SWORDSTATE.ID);
			cli.allowConnection();
			game.enterState(SwordLauncher.StateList.SWORDSTATE.ID);

		}

		// Prevent cursor move
		textField.setCursorPos(MAXSIZE);

		// Set text field focus
		if (!textField.hasFocus())
			textField.setFocus(true);

		// Escape key
		if (input.isKeyPressed(Input.KEY_ESCAPE))
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
	}

	
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		// Call super method
		super.render(gc, game, g);

		// Text field
		g.resetFont();
		g.resetLineWidth();
		g.setColor(Color.white);
		g.setLineWidth(4.0f);
		textField.render(gc, g);

		// Title
		int titleWidth = fontTitle.getWidth(TITLE_TEXT);
		int titleHeight = fontTitle.getHeight(TITLE_TEXT);

		fontTitle.drawString(gc.getWidth() / 2 - titleWidth / 2, gc.getHeight()
				/ 2 - titleHeight - 64.0f, TITLE_TEXT, Color.yellow);

	}


	@Override
	public int getID() {
		return SwordLauncher.StateList.CONNEXIONSTATE.ID;
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame game)
			throws SlickException {
		super.enter(gc, game);
		textField.setText(SwordLauncher.serverIP);
	}

}
