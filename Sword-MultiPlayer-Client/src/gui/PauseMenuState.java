package gui;

import game.SwordScalable;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import client.SwordClientScalable;

public class PauseMenuState extends AbstractMenuState {

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		super.init(arg0, arg1);

		this.setMenuTitle("Pause");
		this.addMenuItem("Return to game", true);
		this.addMenuItem("Quit the game", true);

	}

	@Override
	protected void indexSelectedEvent(int index, StateBasedGame game) {
		switch (index) {
			case 0 :
				if(SwordScalable.isSimpleGameRunning){
					game.enterState(SwordLauncher.StateList.SIMPLEGAME.ID);
				} else {
					game.enterState(SwordLauncher.StateList.SWORDSTATE.ID);
				}
				break;
			case 1 :
				SwordScalable.isSimpleGameRunning=false;
				((SwordClientScalable)game.getState(SwordLauncher.StateList.SWORDSTATE.ID)).endClient();
				game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
				break;
			case -1 :
				SwordScalable.isSimpleGameRunning=false;
				//game.enterState(SwordLauncher.StateList.SWORDSTATE.ID);
				break;
		}
	}

	@Override
	public int getID() {
		return SwordLauncher.StateList.PAUSESTATE.ID;
	}

}
