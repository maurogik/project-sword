package gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tools.SwordFont;

public class HelpSate extends AbstractState {

	private static final String TITLE_TEXT = "Help";
	private static String player1Control = "Main player control :\n Z,Q,S,D to move, space to hit, ctrl to block";
	private static String player2Control = "Main player control :\n Up,Left,Right,Down to move, enter to hit, + to block";
	private static String gameTips = "Game tips :\n Try to score as much points as possible, \n" +
			"there is no maximum score so just stop whenever you get bored.\n" +
			"Press Maj+1 to activate/deactivate AI for player 1, same with MAJ+2 for player 2\n" +
			"You can hit and block up and down by pressing Up/Z or Down/S when you block or attack";
	private Font font, fontTitle;

	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		// Call super method
		super.init(gc, game);

		// Fonts
		font = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Small);
		fontTitle = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Large);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		// Call super method
		super.update(gc, game, delta);
		Input input = gc.getInput();

		// Validation
		if (input.isKeyPressed(Input.KEY_ENTER)) {
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
		}

		// Escape key
		if (input.isKeyPressed(Input.KEY_ESCAPE))
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		// Call super method
		super.render(gc, game, g);

		// Text field
		g.resetFont();
		g.resetLineWidth();
		g.setColor(Color.white);
		g.setLineWidth(4.0f);

		// Title
		int titleWidth = fontTitle.getWidth(TITLE_TEXT);
		int titleHeight = fontTitle.getHeight(TITLE_TEXT);

		//Draw title
		fontTitle.drawString(gc.getWidth() / 2 - titleWidth / 2, gc.getHeight()
				/ 2 - titleHeight - 160.0f, TITLE_TEXT, Color.yellow);
		//Draw tips
		int lineNumber=0;
		
		for(String line: player1Control.split("\n")){
			font.drawString(gc.getWidth() / 2 - font.getWidth(line) / 2, gc.getHeight() / 2
					- font.getHeight(line) +20*lineNumber, line, Color.orange);
			++lineNumber;
		}
		++lineNumber;

		for(String line: player2Control.split("\n")){
			font.drawString(gc.getWidth() / 2 - font.getWidth(line) / 2, gc.getHeight() / 2
					- font.getHeight(line) +20*lineNumber, line, Color.orange);
			++lineNumber;
		}
		++lineNumber;
		
		for(String line: gameTips.split("\n")){
			font.drawString(gc.getWidth() / 2 - font.getWidth(line) / 2, gc.getHeight() / 2
					- font.getHeight(line) +20*lineNumber, line, Color.orange);
			++lineNumber;
		}

	}

	@Override
	public void enter(GameContainer gc, StateBasedGame game)
			throws SlickException {
		super.enter(gc, game);
	}
	@Override
	public int getID() {
		return SwordLauncher.StateList.HELPSTATE.ID;
	}

}
