package gui;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class ConnexionErrorState extends AbstractMenuState{

	
	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		super.init(arg0, arg1);

		this.setMenuTitle("connexion error");
		this.addMenuItem("return to main menu", true);

	}
	
	@Override
	protected void indexSelectedEvent(int index, StateBasedGame game) {
		switch(index){
			case 0 : game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
				break;
			default : game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
				break;
		}
	}

	@Override
	public int getID() {
		return SwordLauncher.StateList.CONNEXIONERRORSTATE.ID;
	}

}
