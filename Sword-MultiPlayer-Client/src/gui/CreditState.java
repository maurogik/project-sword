package gui;

import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import tools.SwordFont;

public class CreditState extends AbstractState{

	private static final String TITLE_TEXT = "Credit";
	private static String message="Gwenn AUBERT :\n" +
			"Game design\n" +
			"Programming\n" +
			"Art and animations\n \n" +
			"Music from www.incompetech.com\n" +
			"Sounds from www.audiomicro.com\n\n" +
			"thanks to Thomas and Matthieu for their help";

	private Font font, fontTitle;

	
	public CreditState(){
		super();
	}
	
	@Override
	public void init(GameContainer gc, StateBasedGame game)
			throws SlickException {
		// Call super method
		super.init(gc, game);

		// Fonts
		font = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Medium);
		fontTitle = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Large);
		
	}

	@Override
	public void update(GameContainer gc, StateBasedGame game, int delta)
			throws SlickException {
		// Call super method
		super.update(gc, game, delta);
		Input input = gc.getInput();

		// Validation
		if (input.isKeyPressed(Input.KEY_ENTER)) {
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
		}

		// Escape key
		if (input.isKeyPressed(Input.KEY_ESCAPE))
			game.enterState(SwordLauncher.StateList.MENUSTATE.ID);
	}

	
	@Override
	public void render(GameContainer gc, StateBasedGame game, Graphics g)
			throws SlickException {
		// Call super method
		super.render(gc, game, g);

		// Text field
		g.resetFont();
		g.resetLineWidth();
		g.setColor(Color.white);
		g.setLineWidth(4.0f);

		// Title
		int titleWidth = fontTitle.getWidth(TITLE_TEXT);
		int titleHeight = fontTitle.getHeight(TITLE_TEXT);
		
		fontTitle.drawString(gc.getWidth() / 2 - titleWidth / 2, gc.getHeight()
				/ 2 - titleHeight - 64.0f, TITLE_TEXT, Color.yellow);
		//Credits
		int nbLine=0;
		
		for(String line : message.split("\n")){
			font.drawString(gc.getWidth() / 2 - font.getWidth(line) / 2, gc.getHeight()
					/ 2 - font.getHeight(line) + 30.0f*nbLine, line, Color.orange);
			++nbLine;
		}
		
	}
	
	@Override
	public void enter(GameContainer gc, StateBasedGame game)
			throws SlickException {
		super.enter(gc, game);
	}
	
	@Override
	public int getID() {
		return SwordLauncher.StateList.CREDITSTATE.ID;
	}
	
}
