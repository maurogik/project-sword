package client;

import java.io.IOException;
import java.io.ObjectInputStream;

import common.PlayersData;
/**
 * The client link class <br>
 * This part is waiting for the server update and patch them through to the game client<br>
 * This class has to be run on a separate thread because when it is waiting for the server data, it can do anything else
 * @author Gwenn
 *
 */
public class ClientLink implements Runnable{

	/**The stream to read the server data*/
	private ObjectInputStream in = null;
	/**The client to keep up to date*/
	private SwordClientScalable client=null;
	/**Stopping the link or not*/
	public boolean stop=false;
	
	/**
	 * Create a new ClientLink
	 * @param cli the client to update
	 * @param input the stream from the server
	 */
	public ClientLink(SwordClientScalable cli,ObjectInputStream input) {
		in=input;
		client=cli;
	}

	@Override
	public void run() {
		
		while(!stop){
			
			if(in!=null && client!=null){
				
				try {
					PlayersData data =(PlayersData)in.readObject();
					client.updateData(data);
				} catch (IOException e) {
					//In case of error, end the link
					return;
				} catch (ClassNotFoundException e) {
					return;
				}
			}
		}
	}
}
