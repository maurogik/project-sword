package client;

import gui.AbstractState;
import gui.SwordLauncher;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import common.*;
import common.Character;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import tools.SwordFont;
import tools.SwordFont.FontName;
import tools.SwordFont.FontSize;
/**
 * The Sword client class<br>
 * Run the game from the client end
 * 
 * @author Gwenn
 * 
 */
public class SwordClientScalable extends AbstractState {

	/**
	 * The Scale enum allow the game to loads diferrent image according to the
	 * chosen scale
	 */
	public static enum Scale {
		Small(0.5f) {
			{
				guardStraightSource = imgPath + "StandToStraight_Small.png";
				guardUpSource = imgPath + "StandToUp_Small.png";
				guardDownSource = imgPath + "StandToDown_Small.png";
				attackStraightSource = imgPath + "StandToAtt_Small.png";
				attackUpSource = imgPath + "StandToAttUp_Small.png";
				attackDownSource = imgPath + "StandToAttDown_Small.png";
				ForwardSource = imgPath + "Forward_Small.png";
			}
		},
		Normal(1f) {
			{
				guardStraightSource = imgPath + "StandToStraight.png";
				guardUpSource = imgPath + "StandToUp.png";
				guardDownSource = imgPath + "StandToDown.png";
				attackStraightSource = imgPath + "StandToAtt.png";
				attackUpSource = imgPath + "StandToAttUp.png";
				attackDownSource = imgPath + "StandToAttDown.png";
				ForwardSource = imgPath + "Forward.png";
			}
		};

		public String guardStraightSource = null;
		public String guardUpSource = null;
		public String guardDownSource = null;
		public String attackStraightSource = null;
		public String attackUpSource = null;
		public String attackDownSource = null;
		public String ForwardSource = null;

		/** The scale value */
		public float value = -1;

		private Scale(float v) {
			value = v;
		}
	}

	/*--------------------sound loading data-----------------*/
	private static String soundPath = "sources/sound/";
	private String hitSoundPath = soundPath + "hit.ogg";
	private Sound hitSound;

	/*------------------Image loading data--------------------*/
	private static String imgPath = "sources/data/";
	private String backgroundPath = imgPath + "game_background.jpg";
	private String lightONPath = imgPath + "lightON.png";
	private String lightOFFPath = imgPath + "lightOFF.png";
	private Image backgroundImg;
	private Image lightONImg;
	private Image lightOFFImg;

	/*-----------------Characters declaration------------------*/

	/** The first player */
	private Character player1 = null;
	/** The second player */
	private Character player2 = null;

	/*-----------------Rendering data-------------------*/

	/** Hight multiplicator used to adapt the graphics to the user's resolution */
	private float heigthCoef = 1;
	/** Width multiplicator used to adapt the graphics to the user's resolution */
	private float widthCoef = 1;
	/** The relative width, used for screen resolution adaptation */
	private float relativeWidth = 1680;
	/** The relative height, used for screen resolution adaptation */
	private float relativeHeight = 1050;

	/*-----------------------Declaration of the communication objects------------------------*/

	/** The socket used to connect to the server */
	private Socket socket = null;
	/** Data writing object */
	private ObjectOutputStream out = null;
	/** data reading object */
	private ObjectInputStream in = null;
	/** The port used by TCP */
	private static int port = 4444;
	/** The last updated data from the server */
	private PlayersData lastData;

	/*------------Miscelaneous---------------*/

	/** Whether or not this client is the player on the left */
	private boolean isClientForPlayer1;
	/** The font used to draw strings in the game */
	private Font gameFont = null;
	/** The scale of the game, Small or Normal */
	private Scale scale = Scale.Small;
	/**
	 * The ClientLink which is receiving the server's data and updating the
	 * client
	 */
	private ClientLink clientLink;
	/**
	 * Whether or not the connection in allowed in the current state of the game
	 */
	private boolean connectionAllowed;
	/** Whether or not the first server have started the game */
	private boolean gameStarted;

	/**
	 * Create a new game
	 * 
	 * @param title
	 */
	public SwordClientScalable() {
		super();
	}

	/**
	 * Init the game<br>
	 * Load the sounds and images Create the players and the animations
	 */
	@Override
	public void init(GameContainer container, StateBasedGame game)
			throws SlickException {

		// Set the size of one animation frame according to the scale of the
		// game
		int animSize = (int) (600 * scale.value);

		// Compute the floats used to adapt the rendering to the user resolution
		heigthCoef = container.getHeight() / relativeHeight;
		widthCoef = container.getWidth() / relativeWidth;

		// sound loading
		super.initMusic(soundPath + "music.ogg", 0.2f, true);
		hitSound = new Sound(hitSoundPath);

		// image loading
		backgroundImg = new Image(backgroundPath);
		lightOFFImg = new Image(lightOFFPath);
		lightONImg = new Image(lightONPath);

		// Spritesheet loading
		ArrayList<CharacterAnimation> animListPlayer1 = new ArrayList<CharacterAnimation>();

		SpriteSheet sprite = new SpriteSheet(scale.guardStraightSource,
				animSize, animSize);
		CharacterAnimation anim = new CharacterAnimation(sprite, 10);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.guardDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 10);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.guardUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 10);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackStraightSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 30);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 40);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.attackUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 40);
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(scale.ForwardSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, 25);
		animListPlayer1.add(anim);

		anim = anim.getReversedAnimation();
		animListPlayer1.add(anim);

		ArrayList<CharacterAnimation> animListPlayer2 = new ArrayList<CharacterAnimation>();
		ArrayList<CharacterAnimation> reversedAnimListPlayer1 = new ArrayList<CharacterAnimation>();
		ArrayList<CharacterAnimation> reversedAnimListPlayer2 = new ArrayList<CharacterAnimation>();

		for (int i = 0; i < animListPlayer1.size(); i++) {
			// Add the reversed animation for player 1
			reversedAnimListPlayer1.add(animListPlayer1.get(i)
					.getReversedAnimation());
			// Add the animation for player2
			animListPlayer2.add(animListPlayer1.get(i).getFlippedAnimation());
			// Add the reversed animation for player2
			reversedAnimListPlayer2.add(animListPlayer2.get(i)
					.getReversedAnimation());
		}

		// Characters creation
		player1 = new Character("player 1", new Vector2f(40, 450),
				(int) animSize, animSize, (int) (50), animListPlayer1,
				reversedAnimListPlayer1);
		player1.setColor(Color.green);

		player2 = new Character("player 2", new Vector2f(1000, 450), animSize,
				animSize, (int) (-50), animListPlayer2, reversedAnimListPlayer2);
		player2.setColor(Color.red);

		// Set the game font
		gameFont = SwordFont.getFont(FontName.Batmfa, FontSize.Medium);

		// Initialize game parameter
		connectionAllowed = false;
		lastData = null;
		gameStarted = false;
	}

	/**
	 * 
	 * @param serverIP
	 * @throws IOException
	 */
	public void startConnection(String serverIP) throws IOException {

		connectionAllowed = false;
		gameStarted = false;

		// connect to the server
		socket = new Socket(serverIP, port);
		out = new ObjectOutputStream(socket.getOutputStream());
		in = new ObjectInputStream(socket.getInputStream());

		Boolean player1;
		try {
			player1 = (Boolean) in.readObject();
			isClientForPlayer1 = player1.booleanValue();
			clientLink = new ClientLink(this, in);
			ExecutorService executor;
			executor = Executors.newFixedThreadPool(2);
			// The client link is runned in a separate thread
			executor.execute(clientLink);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Update the client data with the ones sent by the server
	 * 
	 * @param data
	 *            the server's data
	 */
	public void updateData(PlayersData data) {

		// The first communication with the server has been made, the game start
		// now
		gameStarted = true;

		player1.setLocation(new Vector2f(data.player1location.x,
				data.player1location.y));
		player2.setLocation(new Vector2f(data.player2location.x,
				data.player2location.y));

		player1.setCurrentAnim(data.player1AnimationID,
				data.player1CurrentAnimIsReversed);

		// the correctionOffset is used to compensate the communication lag
		int correctionOffset = 0;
		int time = lastData == null ? data.time : lastData.time;

		if (player1.getCurrentAnim() != null) {

			// Compute the time the packet take to arrive as an offset
			// indicating how further than requested should the animation go
			correctionOffset = (data.time - time)
					/ player1.getCurrentAnim().getDuration(0);

			player1.getCurrentAnim().stopAt(
					Math.min(data.player1AnimationFrame + correctionOffset,
							player1.getCurrentAnim().getFrameCount()));

			player1.getCurrentAnim()
					.setCurrentFrame(data.player1AnimationFrame);

		}

		player2.setCurrentAnim(data.player2AnimationID,
				data.player2CurrentAnimIsReversed);

		if (player2.getCurrentAnim() != null) {

			// Same as player 1 correctionOffset
			correctionOffset = (data.time - time)
					/ player2.getCurrentAnim().getDuration(0);

			player2.getCurrentAnim().stopAt(
					Math.min(data.player2AnimationFrame + correctionOffset,
							player2.getCurrentAnim().getFrameCount()));

			player2.getCurrentAnim()
					.setCurrentFrame(data.player2AnimationFrame);

		}

		player1.setScore(data.player1score);
		player2.setScore(data.player2score);

		if (data.isGamePaused) {
			pause();
		} else {
			unpause();
		}

		// record the data
		lastData = data;
	}

	/**
	 * Send the index of the animation chosen by the player to the server
	 * 
	 * @param index
	 *            the animation index
	 * @throws IOException
	 */
	private void sendPlayerInput(int index) throws IOException {
		out.writeObject((Integer) index);
		out.flush();
	}

	/**
	 * Update the game environment with the user's entries.
	 */
	@Override
	public void update(GameContainer container, StateBasedGame game, int delta)
			throws SlickException {

		Input input = container.getInput();

		boolean gamePaused = lastData == null ? false : lastData.isGamePaused;
		int animationIndex = -1;

		if (!gamePaused && socket != null) {

			if (input.isKeyDown(Input.KEY_ESCAPE)) {
				game.enterState(SwordLauncher.StateList.PAUSESTATE.ID);
			}

			if (player1.isAiActivated()) {
				animationIndex = player1.nextAImove(player2);
			} else {
				if (input.isKeyDown(Input.KEY_SPACE)
						&& input.isKeyDown(Input.KEY_Z)) {
					animationIndex = Character.UP_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_SPACE)
						&& input.isKeyDown(Input.KEY_S)) {
					animationIndex = Character.DOWN_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_SPACE)) {
					animationIndex = Character.STRAIGHT_ATTACK_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)
						&& input.isKeyDown(Input.KEY_Z)) {
					animationIndex = Character.UP_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)
						&& input.isKeyDown(Input.KEY_S)) {
					animationIndex = Character.DOWN_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_LCONTROL)) {
					animationIndex = Character.STRAIGHT_GUARD_ANIMATION_INDEX;

				} else if (input.isKeyDown(Input.KEY_D)) {
					if (isClientForPlayer1) {
						animationIndex = Character.FORWARD_ANIMATION_INDEX;
					} else {
						animationIndex = Character.BACKWARD_ANIMATION_INDEX;
					}
				} else if (input.isKeyDown(Input.KEY_Q)) {
					if (isClientForPlayer1) {
						animationIndex = Character.BACKWARD_ANIMATION_INDEX;
					} else {
						animationIndex = Character.FORWARD_ANIMATION_INDEX;
					}
				}
			}
			try {
				// Send the player's input
				sendPlayerInput(animationIndex);
			} catch (IOException e) {
				/*
				 * In case this doesn't work, that means that the communication
				 * with the server is not possible so we close the client and
				 * return to the main menu
				 */
				e.printStackTrace();
				endClient();
				game.enterState(SwordLauncher.StateList.CONNEXIONERRORSTATE.ID);
			}
		}

	}

	/**
	 * Render the game environment
	 */
	@Override
	public void render(GameContainer container, StateBasedGame game, Graphics g)
			throws SlickException {

		g.setBackground(Color.darkGray);
		backgroundImg.draw(0, 0, container.getWidth(), container.getHeight());
		drawCurrentAnimation(player1, g);
		drawCurrentAnimation(player2, g);
		drawGUI(g);
		playSounds();
	}

	/**
	 * Play the game sound if needed
	 */
	private void playSounds() {
		if (lastData != null && (lastData.player2hit || lastData.player1hit)) {
			if (!hitSound.playing()) {
				hitSound.play(1.0f, 0.8f);
			}
		}
	}

	/**
	 * Draw the Graphical User Interface<br>
	 * Draw the lights<br>
	 * Draw the strings
	 * 
	 * @param g
	 *            the graphical context
	 */
	private void drawGUI(Graphics g) {

		int lightSize = 100;
		String message = "";
		Color textColor = Color.white;
		Color neutral = Color.lightGray;
		g.setColor(neutral);

		boolean gamePaused = lastData == null ? false : lastData.isGamePaused;

		if (gamePaused) {

			if (lastData != null && lastData.player1hit && lastData.player2hit) {
				g.setColor(Color.white);
				g.fillOval(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightONImg.draw(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightONImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef,
						lightSize * widthCoef, lightSize * heigthCoef);
				message = "Draw ! No points awarded.";
			} else if (lastData != null && lastData.player1hit) {
				g.setColor(neutral);
				g.fillOval(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				g.setColor(player2.getColor());
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightOFFImg.draw(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightONImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef,
						lightSize * widthCoef, lightSize * heigthCoef);
				message = "player 1 is hit ! player 2 gain one point.";
				textColor = player2.getColor();
			} else if (lastData != null && lastData.player2hit) {
				g.setColor(player1.getColor());
				g.fillOval(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				g.setColor(neutral);
				g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightONImg.draw(50 * widthCoef, 50 * heigthCoef, lightSize
						* widthCoef, lightSize * heigthCoef);
				lightOFFImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef,
						lightSize * widthCoef, lightSize * heigthCoef);
				message = "player 2 is hit ! player 1 gain one point.";
				textColor = player1.getColor();
			}
		} else {
			g.fillOval(50 * widthCoef, 50 * heigthCoef, lightSize * widthCoef,
					lightSize * heigthCoef);
			g.fillOval((1680 - 170) * widthCoef, 50 * heigthCoef, lightSize
					* widthCoef, lightSize * heigthCoef);
			lightOFFImg.draw(50 * widthCoef, 50 * heigthCoef, lightSize
					* widthCoef, lightSize * heigthCoef);
			lightOFFImg.draw((1680 - 170) * widthCoef, 50 * heigthCoef,
					lightSize * widthCoef, lightSize * heigthCoef);
		}

		message = gameStarted ? message : "Waiting for other player";

		gameFont.drawString(
				(relativeWidth / 2 - gameFont.getWidth(message) / 2)
						* widthCoef,
				(relativeHeight / 2 - gameFont.getHeight(message) / 2)
						* heigthCoef, message, textColor);

		String sco = "player  1 : " + player1.getScore() + "\nplayer 2 : "
				+ player2.getScore();
		gameFont.drawString((relativeWidth / 2 - gameFont.getWidth(sco) / 2)
				* widthCoef, 50 * heigthCoef, sco, Color.yellow);

		String aiMes = "";
		if (player1.isAiActivated()) {
			aiMes = "player1 Ai";
			gameFont.drawString(20 * widthCoef, 200 * heigthCoef, aiMes,
					player1.getColor());
		}
		if (player2.isAiActivated()) {
			aiMes = "player2 Ai";
			gameFont.drawString((1680 - gameFont.getWidth(aiMes) - 20)
					* widthCoef, 200 * heigthCoef, aiMes, player2.getColor());
		}

	}

	/**
	 * Draw the current animation of the character
	 * 
	 * @param player
	 *            the player from who the animation must be drawn
	 * @param g
	 *            the graphical context
	 */
	private void drawCurrentAnimation(Character player, Graphics g) {

		if (player.getCurrentAnim() != null) {

			player.getCurrentAnim().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef / scale.value,
					player.getHeigth() * heigthCoef / scale.value);
		} else {
			player.getBaseImage().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef / scale.value,
					player.getHeigth() * heigthCoef / scale.value);
		}
	}

	/**
	 * Pause the game and all of it's players
	 */
	private void pause() {
		player1.pause();
		player2.pause();
	}

	/**
	 * Unpause the game and the players
	 */
	private void unpause() {
		player1.unpause();
		player2.unpause();
	}

	/**
	 * Get the state id
	 */
	@Override
	public int getID() {
		return SwordLauncher.StateList.SWORDSTATE.ID;
	}

	/**
	 * End the client<br>
	 * Close the socket and the communication streams<br>
	 */
	public void endClient() {

		// close the link
		if (clientLink != null) {
			clientLink.stop = true;
		}

		try {
			if (in != null) {
				in.close();
				in = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (out != null) {
				out.close();
				out = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			if (socket != null) {
				socket.close();
				socket = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		clientLink = null;
		connectionAllowed = false;
		lastData = null;
	}

	/**
	 * Triggered whend entering the state<br>
	 * Connect to the server if allowed
	 */
	@Override
	public void enter(GameContainer container, StateBasedGame game)
			throws SlickException {
		super.enter(container, game);

		if (socket == null && connectionAllowed) {
			try {
				startConnection(SwordLauncher.serverIP);
			} catch (UnknownHostException e) {
				endClient();
				game.enterState(SwordLauncher.StateList.CONNEXIONERRORSTATE.ID);
				e.printStackTrace();
			} catch (IOException e) {
				endClient();
				game.enterState(SwordLauncher.StateList.CONNEXIONERRORSTATE.ID);
				e.printStackTrace();
			}
		}
	}

	/**
	 * Allow the client to connect to the server
	 */
	public void allowConnection() {
		connectionAllowed = true;
	}
}
