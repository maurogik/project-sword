package data;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import org.newdawn.slick.geom.Line;

/**
 * Write the data needed for the animation hit box and sword location<br>
 * Small size version
 * @author Gwenn
 *
 */
public class AnimationDataWritingSmall {

	public static void write() {

		ArrayList<Line> bodyHitBoxList = new ArrayList<Line>();
		ArrayList<Integer> hitBoxRadiusList = new ArrayList<Integer>();
		ArrayList<Line> swordLineList = new ArrayList<Line>();

		String filename = null;
		String path = "sources/data/";
		FileOutputStream fos;
		ObjectOutputStream oos;

		float scale=0.5f;
		
		bodyHitBoxList.add(new Line(169, 302, 135, 428));
		bodyHitBoxList.add(new Line(135, 428, 168, 306));
		bodyHitBoxList.add(new Line(172, 304, 140, 410));
		bodyHitBoxList.add(new Line(164, 306, 137, 416));
		bodyHitBoxList.add(new Line(168, 306, 137, 408));
		bodyHitBoxList.add(new Line(176, 306, 148, 406));
		bodyHitBoxList.add(new Line(182, 308, 153, 416));
		bodyHitBoxList.add(new Line(188, 309, 156, 413));
		bodyHitBoxList.add(new Line(196, 308, 166, 418));
		bodyHitBoxList.add(new Line(209, 308, 174, 418));
		bodyHitBoxList.add(new Line(214, 308, 184, 418));
		bodyHitBoxList.add(new Line(218, 309, 185, 417));
		
		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}

		swordLineList.add(new Line(264, 396, 457, 398));
		swordLineList.add(new Line(264, 396, 453, 396));
		swordLineList.add(new Line(269, 393, 460, 396));
		swordLineList.add(new Line(268, 394, 458, 394));
		swordLineList.add(new Line(266, 392, 458, 397));
		swordLineList.add(new Line(274, 394, 465, 397));
		swordLineList.add(new Line(282, 400, 469, 397));
		swordLineList.add(new Line(286, 397, 474, 396));
		swordLineList.add(new Line(294, 396, 484, 397));
		swordLineList.add(new Line(306, 396, 494, 394));
		swordLineList.add(new Line(313, 393, 500, 397));
		swordLineList.add(new Line(317, 396, 505, 396));
		
		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		try {
			filename = "Forward_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "Forward_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "Forward_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		for (int i = 0; i < 12; i++) {
			bodyHitBoxList.add(new Line(169, 306, 134, 422));
		}

		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}

		swordLineList.add(new Line(264, 399, 454, 399));
		swordLineList.add(new Line(262, 401, 454, 400));
		swordLineList.add(new Line(267, 399, 451, 393));
		swordLineList.add(new Line(269, 398, 450, 378));
		swordLineList.add(new Line(270, 400, 441, 362));
		swordLineList.add(new Line(270, 397, 431, 353));
		swordLineList.add(new Line(269, 400, 424, 337));
		swordLineList.add(new Line(265, 406, 402, 313));
		swordLineList.add(new Line(266, 404, 397, 304));
		swordLineList.add(new Line(258, 413, 377, 294));
		swordLineList.add(new Line(258, 418, 352, 287));
		swordLineList.add(new Line(250, 419, 333, 277));

		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		try {
			filename = "StandToStraight_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToStraight_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToStraight_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		bodyHitBoxList.add(new Line(169, 306, 134, 422));
		bodyHitBoxList.add(new Line(174, 303, 138, 423));
		bodyHitBoxList.add(new Line(187, 302, 143, 420));
		bodyHitBoxList.add(new Line(200, 300, 158, 422));
		bodyHitBoxList.add(new Line(209, 304, 163, 424));
		bodyHitBoxList.add(new Line(221, 309, 175, 430));
		bodyHitBoxList.add(new Line(236, 310, 185, 426));
		bodyHitBoxList.add(new Line(221, 323, 188, 436));
		bodyHitBoxList.add(new Line(248, 333, 188, 440));
		bodyHitBoxList.add(new Line(263, 339, 200, 445));
		bodyHitBoxList.add(new Line(272, 350, 198, 444));
		bodyHitBoxList.add(new Line(273, 349, 196, 444));

		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}

		swordLineList.add(new Line(267, 398, 455, 397));
		swordLineList.add(new Line(282, 389, 465, 375));
		swordLineList.add(new Line(291, 385, 473, 351));
		swordLineList.add(new Line(312, 382, 486, 323));
		swordLineList.add(new Line(327, 374, 495, 301));
		swordLineList.add(new Line(347, 367, 503, 276));
		swordLineList.add(new Line(365, 351, 506, 234));
		swordLineList.add(new Line(374, 339, 480, 192));
		swordLineList.add(new Line(389, 355, 531, 242));
		swordLineList.add(new Line(406, 372, 569, 293));
		swordLineList.add(new Line(411, 400, 591, 356));
		swordLineList.add(new Line(416, 399, 598, 393));

		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		try {
			filename = "StandToAtt_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToAtt_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToAtt_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		bodyHitBoxList.add(new Line(169, 310, 134, 426));
		bodyHitBoxList.add(new Line(175, 307, 133, 426));
		bodyHitBoxList.add(new Line(179, 313, 131, 424));
		bodyHitBoxList.add(new Line(188, 334, 135, 430));
		bodyHitBoxList.add(new Line(204, 334, 143, 439));
		bodyHitBoxList.add(new Line(221, 325, 163, 433));
		bodyHitBoxList.add(new Line(242, 284, 186, 392));
		bodyHitBoxList.add(new Line(264, 248, 206, 357));
		bodyHitBoxList.add(new Line(288, 221, 231, 328));
		bodyHitBoxList.add(new Line(303, 207, 244, 315));
		bodyHitBoxList.add(new Line(316, 206, 259, 312));
		bodyHitBoxList.add(new Line(325, 202, 265, 310));
		
		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}
		
		swordLineList.add(new Line(267, 398, 455, 397));
		swordLineList.add(new Line(279, 360, 421, 237));
		swordLineList.add(new Line(261, 345, 374, 200));
		swordLineList.add(new Line(258, 354, 355, 195));
		swordLineList.add(new Line(268, 330, 374, 177));
		swordLineList.add(new Line(290, 309, 451, 212));
		swordLineList.add(new Line(325, 263, 505, 214));
		swordLineList.add(new Line(366, 220, 553, 229));
		swordLineList.add(new Line(396, 200, 584, 231));
		swordLineList.add(new Line(421, 209, 591, 298));
		swordLineList.add(new Line(435, 221, 586, 333));
		swordLineList.add(new Line(449, 227, 582, 358));
		
		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		try {
			filename = "StandToAttUp_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToAttUp_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToAttUp_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		bodyHitBoxList.add(new Line(169, 306, 134, 422));
		bodyHitBoxList.add(new Line(180, 302, 141, 417));
		bodyHitBoxList.add(new Line(193, 312, 154, 412));
		bodyHitBoxList.add(new Line(204, 321, 164, 426));
		bodyHitBoxList.add(new Line(216, 335, 173, 438));
		bodyHitBoxList.add(new Line(228, 347, 184, 448));
		bodyHitBoxList.add(new Line(247, 365, 193, 454));
		bodyHitBoxList.add(new Line(255, 374, 196, 469));
		bodyHitBoxList.add(new Line(264, 389, 194, 469));
		bodyHitBoxList.add(new Line(290, 401, 218, 469));
		bodyHitBoxList.add(new Line(298, 416, 223, 473));
		bodyHitBoxList.add(new Line(314, 429, 247, 486));

		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}
		
		swordLineList.add(new Line(267, 398, 455, 397));
		swordLineList.add(new Line(277, 395, 446, 399));
		swordLineList.add(new Line(286, 404, 457, 393));
		swordLineList.add(new Line(300, 412, 466, 403));
		swordLineList.add(new Line(307, 436, 474, 444));
		swordLineList.add(new Line(323, 439, 492, 439));
		swordLineList.add(new Line(342, 458, 507, 446));
		swordLineList.add(new Line(343, 473, 515, 461));
		swordLineList.add(new Line(362, 485, 531, 454));
		swordLineList.add(new Line(403, 502, 567, 444));
		swordLineList.add(new Line(419, 510, 554, 432));
		swordLineList.add(new Line(456, 499, 597, 387));
		
		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		try {
			filename = "StandToAttDown_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToAttDown_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToAttDown_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		for (int i = 0; i < 12; i++) {
			bodyHitBoxList.add(new Line(169, 306, 134, 422));
		}

		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}

		swordLineList.add(new Line(264, 399, 454, 399));
		swordLineList.add(new Line(274, 393, 458, 352));
		swordLineList.add(new Line(277, 394, 448, 316));
		swordLineList.add(new Line(285, 384, 431, 263));
		swordLineList.add(new Line(287, 375, 387, 215));
		swordLineList.add(new Line(288, 364, 340, 187));
		swordLineList.add(new Line(287, 359, 320, 175));
		swordLineList.add(new Line(284, 353, 313, 166));
		swordLineList.add(new Line(285, 340, 282, 153));
		swordLineList.add(new Line(282, 312, 228, 133));
		swordLineList.add(new Line(273, 290, 184, 126));
		swordLineList.add(new Line(256, 263, 122, 133));
		
		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		try {
			filename = "StandToUp_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToUp_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToUp_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();

		for (int i = 0; i < 12; i++) {
			bodyHitBoxList.add(new Line(169, 306, 134, 422));
		}

		for (int i = 0; i < 12; i++) {
			hitBoxRadiusList.add((int) (30*scale));
		}

		swordLineList.add(new Line(264, 399, 454, 399));
		swordLineList.add(new Line(282, 397, 456, 431));
		swordLineList.add(new Line(285, 397, 450, 465));
		swordLineList.add(new Line(283, 401, 435, 493));
		swordLineList.add(new Line(285, 388, 441, 470));
		swordLineList.add(new Line(282, 392, 423, 498));
		swordLineList.add(new Line(280, 399, 400, 528));
		swordLineList.add(new Line(279, 402, 376, 550));
		swordLineList.add(new Line(273, 405, 347, 566));
		swordLineList.add(new Line(266, 407, 316, 574));
		swordLineList.add(new Line(275, 405, 279, 576));
		swordLineList.add(new Line(252, 395, 260, 568));
		
		for(Line l : bodyHitBoxList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}
		
		for(Line l : swordLineList){
			l.set(l.getStart().scale(scale), l.getEnd().scale(scale));
		}

		try {
			filename = "StandToDown_Small.hitbox";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(bodyHitBoxList));
			filename = "StandToDown_Small.hitboxradius";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(hitBoxRadiusList);
			filename = "StandToDown_Small.swordlines";
			fos = new FileOutputStream(path + filename);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(Line2.ListLineToListMyLine(swordLineList));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnimationDataWritingSmall.write();
		System.out.println("writing done");
	}

}
