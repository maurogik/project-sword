package data;

import java.io.Serializable;
import java.util.ArrayList;

import org.newdawn.slick.geom.Line;

/**
 * The Line2 class is used to serialize the Line class from Slick
 * @author Gwenn
 *
 */
public class Line2 implements Serializable {

	private static final long serialVersionUID = 3080104614196960036L;
	float X1;
	float Y1;
	float X2;
	float Y2;

	public Line2(float x1, float y1, float x2, float y2) {
		X1 = x1;
		Y1 = y1;
		X2 = x2;
		Y2 = y2;
	}

	/**
	 * Translate from a Line list to a Line2 list
	 * @param list
	 * @return
	 */
	public static ArrayList<Line2> ListLineToListMyLine(
			ArrayList<org.newdawn.slick.geom.Line> list) {
		ArrayList<Line2> res = new ArrayList<Line2>();

		for (Line p : list) {

			res.add(new Line2(p.getX1(), p.getY1(), p.getX2(), p.getY2()));

		}
		return res;
	}

	/**
	 * Translate from a Line2 list to aLine list
	 * @param list
	 * @return
	 */
	public static ArrayList<org.newdawn.slick.geom.Line> ListMyLineToListLine(
			ArrayList<Line2> list) {

		ArrayList<org.newdawn.slick.geom.Line> res = new ArrayList<org.newdawn.slick.geom.Line>();

		for (Line2 p : list) {

			res.add(MyLineToLine(p));
		}
		return res;
	}

	public static org.newdawn.slick.geom.Line MyLineToLine(Line2 ml) {
		return new org.newdawn.slick.geom.Line(ml.X1, ml.Y1, ml.X2, ml.Y2);
	}
}
