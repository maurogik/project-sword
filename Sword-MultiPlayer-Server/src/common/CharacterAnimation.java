package common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import org.newdawn.slick.Animation;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Line;

/**
 * The character animation class<br>
 * Holds every data needed to run the animation and to<br>
 * tell if the character is hit a certain point of the animation
 * 
 * @author Gwenn
 * 
 */
public class CharacterAnimation extends Animation {

	/**
	 * The list of the lines representing the hit box of the body for each
	 * frames
	 */
	private ArrayList<Line> bodyHitBoxList = null;
	/** The list of the radius for the hit box of the body for each frames */
	private ArrayList<Integer> hitBoxRadiusList = null;
	/**
	 * The list of the lines representing the location of the sword for each
	 * frames
	 */
	private ArrayList<Line> swordLineList = null;
	/**
	 * Additionnal pause indicator, used to pause the animation that can be hold
	 * (e.g. guards...)
	 */
	private boolean isPaused = false;

	/**
	 * Standard constructor of the character animation
	 * 
	 * @param sprite
	 *            the animation sprite
	 * @param duration
	 *            the duration of each frame
	 */
	public CharacterAnimation(SpriteSheet sprite, int duration) {
		super(sprite, duration);
		this.setLooping(false);
		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();
		readAnimationData(sprite.getResourceReference());
	}

	/**
	 * Constructor of an empty CharacterAnimation object
	 */
	private CharacterAnimation() {
		super();
		this.setLooping(false);
		bodyHitBoxList = new ArrayList<Line>();
		hitBoxRadiusList = new ArrayList<Integer>();
		swordLineList = new ArrayList<Line>();
	}

	/**
	 * Read the data needed for the hitbox calculation in the files
	 * 
	 * @param filename
	 */
	@SuppressWarnings("unchecked")
	private void readAnimationData(String filename) {
		// Loading the animation data for the hitbox
		filename = filename.substring(0, filename.lastIndexOf('.'));
		filename += ".hitbox";

		try {
			FileInputStream fos = new FileInputStream(filename);
			ObjectInputStream out = new ObjectInputStream(fos);

			bodyHitBoxList = data.Line2
					.ListMyLineToListLine((ArrayList<data.Line2>) out
							.readObject());
			hitBoxRadiusList = new ArrayList<Integer>();
			swordLineList = new ArrayList<Line>();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// Loading animation data for the hit box radius
		filename = filename.substring(0, filename.lastIndexOf('.'));
		filename += ".hitboxradius";

		try {
			FileInputStream fos = new FileInputStream(filename);
			ObjectInputStream out = new ObjectInputStream(fos);

			hitBoxRadiusList = (ArrayList<Integer>) out.readObject();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		// Loading animation data for the sword lines
		filename = filename.substring(0, filename.lastIndexOf('.'));
		filename += ".swordlines";

		try {
			FileInputStream fos = new FileInputStream(filename);
			ObjectInputStream out = new ObjectInputStream(fos);

			swordLineList = data.Line2
					.ListMyLineToListLine((ArrayList<data.Line2>) out
							.readObject());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the line of the ennemy sword
	 * 
	 * @return a Line object representing ennemy sword
	 */
	public Line getSword() {
		return swordLineList.get(super.getFrame());
	}

	/**
	 * Get the hit box radius for this animation
	 * 
	 * @return
	 */
	public int getRadius() {
		return (int) ((float) hitBoxRadiusList.get(super.getFrame()) / 1.5f);
	}

	/**
	 * Get the line representing the hit box for this animation
	 * 
	 * @return
	 */
	public Line getHitBox() {
		return bodyHitBoxList.get(super.getFrame());
	}

	/**
	 * Create and return the same animation but with the order of the frames
	 * reversed
	 * 
	 * @return a reversed animation
	 */
	public CharacterAnimation getReversedAnimation() {
		CharacterAnimation revAnim = new CharacterAnimation();

		// Fill the null animation with inverted data from the current animation
		for (int i = this.getFrameCount() - 1; i >= 0; i--) {
			revAnim.addFrame(this.getImage(i), this.getDuration(i));

			revAnim.bodyHitBoxList.add(this.bodyHitBoxList.get(i));
			revAnim.hitBoxRadiusList.add(this.hitBoxRadiusList.get(i));
			revAnim.swordLineList.add(this.swordLineList.get(i));

		}
		return revAnim;
	}

	/**
	 * Contruct and return the mirrored animation
	 * 
	 * @return a mirror animation
	 */
	public CharacterAnimation getFlippedAnimation() {
		CharacterAnimation mirrorAnim = new CharacterAnimation();
		for (int i = 0; i < this.getFrameCount(); i++) {

			mirrorAnim.addFrame(this.getImage(i).getFlippedCopy(true, false),
					this.getDuration(i));

			// Adapt the hit box position to the mirrored image
			Line line = new Line(this.getWidth()
					- this.bodyHitBoxList.get(i).getX1(), this.bodyHitBoxList
					.get(i).getY1(), this.getWidth()
					- this.bodyHitBoxList.get(i).getX2(), this.bodyHitBoxList
					.get(i).getY2());

			mirrorAnim.bodyHitBoxList.add(line);
			mirrorAnim.hitBoxRadiusList.add(this.hitBoxRadiusList.get(i));
			// Adapt the sword position to the mirrored image
			line = new Line(
					this.getWidth() - this.swordLineList.get(i).getX1(),
					this.swordLineList.get(i).getY1(), this.getWidth()
							- this.swordLineList.get(i).getX2(),
					this.swordLineList.get(i).getY2());
			mirrorAnim.swordLineList.add(line);

		}
		return mirrorAnim;
	}

	/**
	 * Return whether or not the naimation is in pause mode
	 * 
	 * @return true if paused
	 */
	public boolean isPaused() {
		return isPaused;
	}

	/**
	 * Pause the animation on the current image, do not stop it
	 */
	public void pause() {
		isPaused = true;
		super.stop();
	}

	/**
	 * unpause the animation
	 */
	public void unpause() {
		isPaused = false;
		super.start();
	}

	/**
	 * modified version of the isStopped function<BR>
	 * only return true if the animation is not paused
	 */
	@Override
	public boolean isStopped() {
		return super.isStopped() && !isPaused;
	}

	/**
	 * modified version of the stop function<BR>
	 * also unpause the anim
	 */
	@Override
	public void stop() {
		super.stop();
		unpause();
	}

	/**
	 * modified version of the start function<BR>
	 * also unpause the anim
	 */
	@Override
	public void start() {
		super.start();
		unpause();
	}

	/**
	 * modified version of the restart function<BR>
	 * also unpause the anim
	 */
	@Override
	public void restart() {
		super.restart();
		unpause();
	}
}
