package common;

import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;

public class Character {


	/*---------------Global indexes of the animation in the animation list----------------*/
	public static int STRAIGHT_GUARD_ANIMATION_INDEX = 0;
	public static int DOWN_GUARD_ANIMATION_INDEX = 1;
	public static int UP_GUARD_ANIMATION_INDEX = 2;
	public static int STRAIGHT_ATTACK_ANIMATION_INDEX = 3;
	public static int DOWN_ATTACK_ANIMATION_INDEX = 4;
	public static int UP_ATTACK_ANIMATION_INDEX = 5;
	public static int FORWARD_ANIMATION_INDEX = 6;
	public static int BACKWARD_ANIMATION_INDEX = 7;

	/*--------------Animation data---------------*/

	/** list containing all the available animation for the player1 */
	private ArrayList<CharacterAnimation> animList = null;
	/** list containing all the available reversed animation for the player1 */
	private ArrayList<CharacterAnimation> reversedAnimList = null;
	/** The heigth of one frame */
	private int heigth;
	/** The width of one frame */
	private int width;

	/*---------------Character data--------------------*/

	/** The character's name */
	private String name;
	/** the character current animation */
	private CharacterAnimation currentAnim = null;
	/** the player location (from where to draw the current animation) */
	private Vector2f location = null;
	/** The starting location of the character */
	private Vector2f startingLocation = null;
	/** Wether or not a reversed animation is runned by this character */
	private boolean reversedAnimRunning = false;
	/** the change in the character's location at each of the character's moves */
	private int mouvmentOffset = 0;
	/** Holds the score for the player */
	private int score = 0;
	/** The color of the player's GUI part */
	private Color color = Color.white;
	/** The next move this character will perform */
	private int nextMove;

	/**
	 * 
	 * Create a new Character object, initialize the fields
	 * 
	 * @param name
	 *            the character's name
	 * @param loc
	 *            le starting location of the character
	 * @param height
	 *            the height of the character display
	 * @param width
	 *            the width of the character display
	 * @param mouvmentOffset
	 *            the change in the character's location at each of his moves
	 * @param animL
	 *            The list of animation for this character
	 * @param revAnimL
	 *            This list of reversed animation for this character
	 */
	public Character(String name, Vector2f loc, int height, int width,
			int mouvmentOffset, ArrayList<CharacterAnimation> animL,
			ArrayList<CharacterAnimation> revAnimL) {

		this.name = name;
		this.location = loc;
		this.startingLocation = loc;
		this.width = width;
		this.heigth = height;
		this.mouvmentOffset = mouvmentOffset;
		animList = animL;
		reversedAnimList = revAnimL;
		nextMove = -1;
	}

	/**
	 * Get the name of the character
	 * 
	 * @return The name as a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Return the currently displayed, or last displayed animation for this
	 * character
	 * 
	 * @return the current animation
	 */
	public CharacterAnimation getCurrentAnim() {
		return currentAnim;
	}

	/**
	 * Set the currently displayed animation.
	 * 
	 * @param anim
	 *            the animation to display
	 */
	public void setCurrentAnim(CharacterAnimation anim) {
		this.currentAnim = anim;
	}

	/**
	 * Set the currently displayer animation
	 * 
	 * @param ind
	 *            the index of the animation
	 */
	public void setCurrentAnim(int ind) {
		currentAnim = animList.get(ind);
	}

	/**
	 * Set the currently displayer animation
	 * 
	 * @param ind
	 *            the index of the animation
	 * @param reversedAnim
	 *            If the animation index is concerning a reversed animation or
	 *            not
	 */
	public void setCurrentAnim(int ind, boolean reversedAnim) {
		if (ind == -1) {
			currentAnim = null;
		} else if (!reversedAnim) {
			currentAnim = animList.get(ind);
		} else {
			currentAnim = reversedAnimList.get(ind);
		}

	}

	/**
	 * Get the location of a character
	 * 
	 * @return The location a a vertor of two floats
	 */
	public Vector2f getLocation() {
		return location;
	}

	/**
	 * Set the player location
	 * 
	 * @param location
	 *            the new location
	 */
	public void setLocation(Vector2f location) {
		this.location = location;
	}

	public int getHeigth() {
		return heigth;
	}

	public int getWidth() {
		return width;
	}

	/**
	 * 
	 * @return true if the character is currently running a reversed animation
	 */
	public boolean isReversedAnimRunning() {
		return reversedAnimRunning;
	}

	/**
	 * Set if the character is going to be running a reversed animation
	 * 
	 * @param reversedAnimRunning
	 */
	public void setReversedAnimRunning(boolean reversedAnimRunning) {
		this.reversedAnimRunning = reversedAnimRunning;
	}

	/**
	 * Return the mouvment offset
	 * 
	 * @return
	 */
	public int getMouvmentOffset() {
		return mouvmentOffset;
	}

	/**
	 * Update the player animation and data according to the user's entry
	 * 
	 * @param this the player to update
	 * @param animationIndex
	 *            the index of the animation to run
	 * @param animList
	 *            the list where the animation can be gotten
	 * @param reversedAnimList
	 *            the list where the reversed animation can be gotten
	 */
	public void updatePlayer(int animationIndex) {

		/*
		 * If input from the player and the running animation is not reversed,
		 * we can perform the required animation
		 */
		if (animationIndex != -1 && !this.isReversedAnimRunning()) {
			CharacterAnimation anim = animList.get(animationIndex);

			// If the required animation is the already running one
			if (animationIndex == animList.indexOf(this.getCurrentAnim())
					&& anim.getFrame() == anim.getFrameCount() - 1) {
				/*If it is a guard animation, we stop it on the last frame
				  (so the guard stays up)*/
				if (animationIndex <= UP_GUARD_ANIMATION_INDEX) {
					anim.pause();
				} else { 
					//else we will perform the reversed animation later
					animationIndex = -1;
				}
			} else if (this.getCurrentAnim() != null) {
				//we need to perform the reversed animation
				animationIndex = -1;
			} else {
				//we can start the required animation
				startAnimation(animationIndex, this);
			}
		}
		
		//perform the reversed animation or update the player location
		if (animationIndex == -1 && !this.isReversedAnimRunning()) {
			if (animList.indexOf(this.getCurrentAnim()) == FORWARD_ANIMATION_INDEX
					&& this.getCurrentAnim().isStopped()) {
				moveForward();

			} else if (animList.indexOf(this.getCurrentAnim()) == BACKWARD_ANIMATION_INDEX
					&& this.getCurrentAnim().isStopped()) {
				currentAnim = null;

			} else if (this.getCurrentAnim() != null) {
				performReversedAnimation(
						animList.indexOf(this.getCurrentAnim()), false);
			}
		}

		if (this.isReversedAnimRunning() && this.getCurrentAnim().isStopped()) {
			this.setCurrentAnim(null);
			this.setReversedAnimRunning(false);
		}
	}

	/**
	 * Offset the player location and set it's current animation to null
	 */
	private void moveForward() {
		this.setLocation(new Vector2f(this.getLocation().x
				+ this.getMouvmentOffset(), this.getLocation().y));
		this.setCurrentAnim(null);
	}

	/**
	 * Offset the player location and set it's current animation to null
	 */
	private void moveBackward() {
		this.setLocation(new Vector2f(this.getLocation().x
				- this.getMouvmentOffset(), this.getLocation().y));
	}

	/**
	 * Start an animation from the first frame. <br>
	 * 
	 * @param ind
	 *            the index of the animation in the list
	 */
	private void startAnimation(int ind, Character player) {
		animList.get(ind).restart();
		animList.get(ind).start();
		if (ind == BACKWARD_ANIMATION_INDEX) {
			moveBackward();
		}
		player.setCurrentAnim(animList.get(ind));
	}

	/**
	 * Perform the reversed animation for the indth animation. <br>
	 * Start either from an intermediate frame if the animation was running <br>
	 * or from the first frame if the animation had reached its end. <br>
	 * Also stops the corresponding animation.
	 * 
	 * @param ind
	 *            the index of the animation in the list
	 * @param stopCurrent
	 *            boolean stating if the current animation should be forced to
	 *            stop to perform the reversed animation
	 */
	public void performReversedAnimation(int ind, boolean stopCurrent) {

		if (this.currentAnim != null
				&& (this.getCurrentAnim().isStopped()
						|| this.getCurrentAnim().isPaused() || stopCurrent)
				&& ind != -1) {
			this.setReversedAnimRunning(true);

			int frame = animList.get(ind).getFrameCount()
					- animList.get(ind).getFrame() - 1;

			reversedAnimList.get(ind).restart();
			reversedAnimList.get(ind).setCurrentFrame(frame);
			reversedAnimList.get(ind).start();
			this.setCurrentAnim(reversedAnimList.get(ind));
		}
	}

	/**
	 * Get the index of the currently display or last displayed animation<br>
	 * return -1 if the current animation is reversed
	 * 
	 * @return the index of the animation
	 */
	public int getCurrentAnimationIndex() {
		return animList.indexOf(currentAnim);
	}

	/**
	 * Get the basic (standing) image for a character
	 * 
	 * @return the basic image 
	 */
	public Image getBaseImage() {
		return animList.get(0).getImage(0);
	}

	/**
	 * Test is the tip of the ennemy sword is touching the character
	 * 
	 * @param ennemyTip
	 *            the line representing the ennemy sword
	 * @return true if hit
	 */
	public boolean isHitBy(Character ennemy) {

		Vector2f ennemyTip = ennemy.getSwordTip();
		Line ennemySword = ennemy.getSword();
		Line updatedHitBox;
		boolean changed = false;
		int radius = 0;

		//If no animation is running we use the basic image
		if (currentAnim == null) {
			currentAnim = animList.get(0);
			currentAnim.setCurrentFrame(0);
			changed = true;
		}
		
		radius = currentAnim.getRadius();
		updatedHitBox = new Line(currentAnim.getHitBox().getX1() + location.x,
				currentAnim.getHitBox().getY1() + location.y, currentAnim
						.getHitBox().getX2() + location.x, currentAnim
						.getHitBox().getY2() + location.y);

		if (changed) {
			currentAnim = null;
		}

		return (updatedHitBox.distance(ennemyTip) < radius || updatedHitBox
				.intersect(ennemySword, true, new Vector2f()))
				&& ennemy.getCurrentAnimationIndex() >= STRAIGHT_ATTACK_ANIMATION_INDEX
				&& ennemy.getCurrentAnimationIndex() <= UP_ATTACK_ANIMATION_INDEX;
	}

	/**
	 * Test if the character sword is in the way of the ennemy sword
	 * 
	 * @param ennemySword
	 *            the line representing the ennemy sword
	 * @return true if a block is done
	 */
	public boolean isBlocking(Line ennemySword) {
		Vector2f inter = new Vector2f();
		boolean changed = false;

		Line updatedSword;
		//If no animation is running we use the basic image
		if (currentAnim == null) {
			currentAnim = animList.get(0);
			currentAnim.setCurrentFrame(0);
			changed = true;
		}
		
		updatedSword = new Line(currentAnim.getSword().getX1() + location.x,
				currentAnim.getSword().getY1() + location.y, currentAnim
						.getSword().getX2() + location.x, currentAnim
						.getSword().getY2() + location.y);

		boolean intersect = updatedSword.intersect(ennemySword, true, inter);

		if (changed) {
			currentAnim = null;
			return false;
		}

		return intersect
				&& (getCurrentAnimationIndex() == UP_GUARD_ANIMATION_INDEX
						|| getCurrentAnimationIndex() == STRAIGHT_GUARD_ANIMATION_INDEX || getCurrentAnimationIndex() == DOWN_GUARD_ANIMATION_INDEX);
	}

	/**
	 * Get the line of the player's sword
	 * 
	 * @return a Line object representing ennemy sword
	 */
	public Line getSword() {
		Line updatedSword;
		boolean changed = false;
		//If no animation is running we use the basic image
		if (currentAnim == null) {
			currentAnim = animList.get(0);
			currentAnim.setCurrentFrame(0);
			changed = true;
		}
		updatedSword = new Line(currentAnim.getSword().getX1() + location.x,
				currentAnim.getSword().getY1() + location.y, currentAnim
						.getSword().getX2() + location.x, currentAnim
						.getSword().getY2() + location.y);

		if (changed) {
			currentAnim = null;
		}

		return updatedSword;
	}

	/**
	 * Get the pointy end of the sword
	 * 
	 * @return the sword's end as a vector of floats
	 */
	public Vector2f getSwordTip() {
		return new Vector2f(getSword().getX2(), getSword().getY2());
	}

	/**
	 * Pause the currently running animation
	 */
	public void pause() {
		if (currentAnim != null) {
			currentAnim.pause();
		}
	}

	/**
	 * Unpause the currently running animation
	 */
	public void unpause() {
		if (currentAnim != null) {
			currentAnim.unpause();
		}
	}

	/**
	 * Get the starting location of the animation
	 * 
	 * @return
	 */
	public Vector2f getStartingLocation() {
		return startingLocation;
	}

	/**
	 * Set the starting location
	 * 
	 * @param startingLocation
	 */
	public void setStartingLocation(Vector2f startingLocation) {
		this.startingLocation = startingLocation;
	}

	/**
	 * Add a point to this player's score
	 */
	public void addPoint() {
		score++;
	}

	/**
	 * Reset this player's score
	 */
	public void resetScore() {
		score = 0;
	}

	/**
	 * Get this player's score
	 * 
	 * @return the number of point of the player
	 */
	public int getScore() {
		return score;
	}

	/**
	 * Set this player's score
	 * 
	 * @param sco
	 *            the new score
	 */
	public void setScore(int sco) {
		score = sco;
	}

	/**
	 * Reset this player animation and location
	 */
	public void resetPlayer() {
		currentAnim = null;
		location = startingLocation;
		reversedAnimRunning = false;
	}

	/**
	 * Get the GUI color for this player
	 * 
	 * @return
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the GUI color for this player
	 * 
	 * @param color
	 */
	public void setColor(Color color) {
		this.color = color;
	}

	/**
	 * Get the character next move
	 * @return
	 */
	public int getNextMove() {
		return nextMove;
	}

	/**
	 * Set the character nextMove
	 * @param nextMove
	 */
	public void setNextMove(int nextMove) {
		this.nextMove = nextMove;
	}
	
	/**
	 *  Get the current animation index, whether it is a regular or a reversed animation<br>
	 * @return
	 */
	public int getDataAnimationIndex(){
		return reversedAnimRunning?reversedAnimList.indexOf(currentAnim):animList.indexOf(currentAnim);
	}
}
