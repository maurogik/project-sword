package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import common.Character;

/**
 * The InputFetcher class resposible for getting the input from one of the two
 * clients
 * 
 * @author Gwenn
 * 
 */
public class InputFetcher implements Runnable {

	/** The stream from wich the input arrive */
	private ObjectInputStream in = null;
	/** The player sending the input */
	private Character player = null;
	/** To stop or not to stop...that is the question. */
	public boolean stop = false;

	public InputFetcher(Character playerParam, ObjectInputStream input) {
		player = playerParam;
		in = input;
	}

	/**
	 * Wait for the input from the client and patch it to the server
	 */
	@Override
	public void run() {
		int input;

		while (!stop) {

			if (in != null && player != null) {

				try {
					input = (Integer) in.readObject();
					player.setNextMove(input);
				} catch (IOException e) {
					e.printStackTrace();
					return;
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					return;
				}
			} else {
				System.out.println("NO !");
				return;
			}
		}
	}
}
