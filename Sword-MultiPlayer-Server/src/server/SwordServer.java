package server;

import java.awt.Point;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.geom.Vector2f;
import common.Character;
import common.CharacterAnimation;
import common.PlayersData;
import tools.SwordFont;

/**
 * The SwordServer class<br>
 * Run a server wating for two clients to connect<br>
 * If one disconect or an error occur, the server should close
 * 
 * @author Gwenn
 * 
 */
public class SwordServer extends BasicGame {

	/**
	 * The state of the server with a string associated to each state
	 * 
	 * @author Gwenn
	 * 
	 */
	private enum ServerState {
		Wating("Waiting for connection"), Running("Server running"), None("");
		public String message;

		ServerState(String msg) {
			message = msg;
		}
	}

	/*-----------------SpriteSheet sources-------------------*/
	private String guardStraightSource = "sources/data/StandToStraight.png";
	private String guardUpSource = "sources/data/StandToUp.png";
	private String guardDownSource = "sources/data/StandToDown.png";
	private String attackStraightSource = "sources/data/StandToAtt.png";
	private String attackUpSource = "sources/data/StandToAttUp.png";
	private String attackDownSource = "sources/data/StandToAttDown.png";
	private String ForwardSource = "sources/data/Forward.png";

	/*--------------------Rendering data---------------------*/

	/** Hight multiplicator used to adapt the graphics to the user's resolution */
	private float heigthCoef = 1;
	/** Width multiplicator used to adapt the graphics to the user's resolution */
	private float widthCoef = 1;
	/** The relative width, used for screen resolution adaptation */
	private float relativeWidth = 1680;
	/** The relative height, used for screen resolution adaptation */
	private float relativeHeight = 1050;

	/*------------------Communication objects----------------------*/

	/** Socket connected to player1 */
	private Socket socketP1 = null;
	/** Socket connected to player2 */
	private Socket socketP2 = null;
	/** The server socket */
	private ServerSocket serverSocket = null;
	/** Writing stream for player 1 */
	private ObjectOutputStream outP1 = null;
	/** Reading stream sor player 1 */
	private ObjectInputStream inP1 = null;
	/** Writing stream for player 2 */
	private ObjectOutputStream outP2 = null;
	/** Reading stream sor player 2 */
	private ObjectInputStream inP2 = null;
	/** port for TCP to use */
	private int port = 4444;
	/** InputFetcher for player1 */
	private InputFetcher P1InputFetcher;
	/** InputFetcher for player2 */
	private InputFetcher P2InputFetcher;

	/*-------------------Game data----------------------*/

	/** The game timer */
	private int time;
	/** The amount of time spent on pause mode */
	private float pausedTime = 0;
	/** If the game is paused */
	private boolean gamePaused = false;
	/** The first player */
	private Character player1 = null;
	/** The second player */
	private Character player2 = null;
	/** The state of the server */
	private ServerState serverState;
	/** The font with which the message will be displayed */
	private Font messageFont;
	/** Tell wether or not the first update has been gone through */
	private boolean firstUpdate = true;
	/** The server IP address */
	private String hostIP = null;

	/**
	 * Create a new server
	 * 
	 * @param title
	 */
	public SwordServer(String title) {
		super(title);
		serverState = ServerState.None;
	}

	/**
	 * Init the server<br>
	 * Initialize the characters and animations Set the game parameters
	 */
	@Override
	public void init(GameContainer container) throws SlickException {

		double test=1;
		// Rendering data initialization
		int animSize = 600;
		heigthCoef = container.getHeight() / relativeHeight;
		widthCoef = container.getWidth() / relativeWidth;

		// Animation initialization
		ArrayList<CharacterAnimation> animListPlayer1 = new ArrayList<CharacterAnimation>();
		SpriteSheet sprite = new SpriteSheet(guardStraightSource, animSize,
				animSize);
		CharacterAnimation anim = new CharacterAnimation(sprite, (int) (20*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(guardDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (20*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(guardUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (20*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(attackStraightSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (60*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(attackDownSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (80*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(attackUpSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (80*test));
		animListPlayer1.add(anim);

		sprite = new SpriteSheet(ForwardSource, animSize, animSize);
		anim = new CharacterAnimation(sprite, (int) (50*test));
		animListPlayer1.add(anim);

		anim = anim.getReversedAnimation();
		animListPlayer1.add(anim);

		ArrayList<CharacterAnimation> animListPlayer2 = new ArrayList<CharacterAnimation>();

		ArrayList<CharacterAnimation> reversedAnimListPlayer1 = new ArrayList<CharacterAnimation>();
		ArrayList<CharacterAnimation> reversedAnimListPlayer2 = new ArrayList<CharacterAnimation>();

		for (int i = 0; i < animListPlayer1.size(); i++) {
			// Add the reversed animation for player 1
			reversedAnimListPlayer1.add(animListPlayer1.get(i)
					.getReversedAnimation());
			// Add the animation for player2
			animListPlayer2.add(animListPlayer1.get(i).getFlippedAnimation());
			// Add the reversed animation for player2
			reversedAnimListPlayer2.add(animListPlayer2.get(i)
					.getReversedAnimation());
		}

		// Character creation
		player1 = new Character("player 1", new Vector2f(40, 300), animSize,
				animSize, 50, animListPlayer1, reversedAnimListPlayer1);
		player1.setColor(Color.green);

		player2 = new Character("player 2", new Vector2f(1000, 305), animSize,
				animSize, -50, animListPlayer2, reversedAnimListPlayer2);
		player2.setColor(Color.red);

		// Game data initialization
		P1InputFetcher = null;
		P2InputFetcher = null;

		messageFont = SwordFont.getFont(SwordFont.FontName.Batmfa,
				SwordFont.FontSize.Small);
		serverState = ServerState.Wating;

		try {
			hostIP = java.net.InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			hostIP = "no ip found";
			e.printStackTrace();
		}
	}

	/**
	 * Start the server<br>
	 * Wait for the two clients to connect Initialize the communication objects
	 */
	private void startServer() {

		serverState = ServerState.Wating;
		try {

			serverSocket = new ServerSocket(port);

			socketP1 = serverSocket.accept();
			outP1 = new ObjectOutputStream(socketP1.getOutputStream());
			inP1 = new ObjectInputStream(socketP1.getInputStream());
			Boolean BolPlayer1 = true;
			outP1.writeObject(BolPlayer1);

			socketP2 = serverSocket.accept();

			outP2 = new ObjectOutputStream(socketP2.getOutputStream());
			inP2 = new ObjectInputStream(socketP2.getInputStream());
			BolPlayer1 = false;
			outP2.writeObject(BolPlayer1);

			P1InputFetcher = new InputFetcher(player1, inP1);
			P2InputFetcher = new InputFetcher(player2, inP2);

			ExecutorService executor = Executors.newFixedThreadPool(2);
			executor.execute(P1InputFetcher);
			executor.execute(P2InputFetcher);

			serverState = ServerState.Running;
			time = 0;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send the up to date data to the client
	 */
	private void sendData() {

		// Create and fill up the Data to send
		PlayersData dat = new PlayersData();

		dat.player1location = new Point((int) player1.getLocation().x,
				(int) player1.getLocation().y);
		dat.player2location = new Point((int) player2.getLocation().x,
				(int) player2.getLocation().y);

		dat.player1AnimationID = player1.getDataAnimationIndex();
		dat.player2AnimationID = player2.getDataAnimationIndex();

		dat.player1CurrentAnimIsReversed = player1.isReversedAnimRunning();
		dat.player2CurrentAnimIsReversed = player2.isReversedAnimRunning();

		dat.player1AnimationFrame = player1.getCurrentAnim() == null
				? 0
				: player1.getCurrentAnim().getFrame();
		dat.player2AnimationFrame = player2.getCurrentAnim() == null
				? 0
				: player2.getCurrentAnim().getFrame();

		dat.player1score = player1.getScore();
		dat.player2score = player2.getScore();

		dat.isGamePaused = gamePaused;

		dat.player1hit = player1.isHitBy(player2)
				&& player2.getCurrentAnim() != null;
		dat.player2hit = player2.isHitBy(player1)
				&& player1.getCurrentAnim() != null;

		dat.time = time;

		try {
			outP1.writeObject(dat);
			outP1.flush();

		} catch (IOException e) {
			endServer();
		}

		try {
			outP2.writeObject(dat);
			outP2.flush();
		} catch (IOException e) {
			endServer();
		}
	}

	/**
	 * Update the game environment with the user's entries.
	 */
	@Override
	public void update(GameContainer container, int delta)
			throws SlickException {

		if (firstUpdate) {
			startServer();
			firstUpdate = false;
		}

		time += delta;

		Input input = container.getInput();
		if (input.isKeyDown(Input.KEY_P)) {
			unpause();
			resetPlayersPosition();
		}

		int animationIndexPlayer1 = player1.getNextMove();
		int animationIndexPlayer2 = player2.getNextMove();

		if (!gamePaused) {

			pausedTime = 0;

			if (// Fobid to move further than the other player
			!(animationIndexPlayer1 == Character.FORWARD_ANIMATION_INDEX && player1
					.getLocation().x + player1.getMouvmentOffset() > player2
					.getLocation().x - 150)
			// Forbid to go out of the screen
					&& !(animationIndexPlayer1 == Character.BACKWARD_ANIMATION_INDEX && player1
							.getLocation().x - player1.getMouvmentOffset() < 0)) {

				player1.updatePlayer(animationIndexPlayer1);
			}

			if (// Fobid to move further than the other player
			!(animationIndexPlayer2 == Character.FORWARD_ANIMATION_INDEX && player2
					.getLocation().x + player2.getMouvmentOffset() < player1
					.getLocation().x + 150)
			// Forbid to go out of the screen
					&& !(animationIndexPlayer2 == Character.BACKWARD_ANIMATION_INDEX && player2
							.getLocation().x
							+ player2.getBaseImage().getWidth()
							- player2.getMouvmentOffset() > relativeWidth)) {

				player2.updatePlayer(animationIndexPlayer2);
			}

			// Tests for hit and block
			if (player1.isHitBy(player2) && player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null
					&& player2.getCurrentAnim() != null) {
				pause();
			} else if (player1.isHitBy(player2)
					&& player2.getCurrentAnim() != null) {
				player2.addPoint();
				pause();
			} else if (player2.isHitBy(player1)
					&& player1.getCurrentAnim() != null) {
				player1.addPoint();
				pause();
			} else if (player1.isBlocking(player2.getSword())
					&& player2.isBlocking(player1.getSword())) {
				player1.performReversedAnimation(
						player1.getCurrentAnimationIndex(), true);
				player2.performReversedAnimation(
						player2.getCurrentAnimationIndex(), true);
			} else if (player2.isBlocking(player1.getSword())) {
				player1.performReversedAnimation(
						player1.getCurrentAnimationIndex(), true);
			} else if (player1.isBlocking(player2.getSword())) {
				player2.performReversedAnimation(
						player2.getCurrentAnimationIndex(), true);
			}
			sendData();
		} else {
			pausedTime += delta;
			if (pausedTime > 2000) {
				unpause();
				resetPlayersPosition();
			}
		}

		if (socketP1.isClosed() || socketP2.isClosed()) {
			endServer();
		}
	}

	/**
	 * Render the game environment
	 */
	@Override
	public void render(GameContainer container, Graphics g)
			throws SlickException {

		g.setBackground(Color.white);
		drawCurrentAnimation(player1, g);
		drawCurrentAnimation(player2, g);
		printServerSate(container, g);

	}

	/**
	 * Print the actual server state
	 * @param container the game container
	 * @param g the graphical context
	 */
	private void printServerSate(GameContainer container, Graphics g) {
		int msgWidth = messageFont.getWidth(serverState.message);
		int msgHeight = messageFont.getHeight(serverState.message);

		messageFont.drawString(container.getWidth() / 2 - msgWidth / 2,
				container.getHeight() / 2 - msgHeight - 64.0f,
				serverState.message, Color.red);

		String msgIP = "Server ip : " + hostIP;

		messageFont.drawString(
				container.getWidth() / 2 - messageFont.getWidth(msgIP) / 2,
				container.getHeight() / 2 - messageFont.getHeight(msgIP),
				msgIP, Color.red);
	}

	/**
	 * Draw the current animation of the character
	 */
	private void drawCurrentAnimation(Character player, Graphics g) {

		if (player.getCurrentAnim() != null) {

			player.getCurrentAnim().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef,
					player.getHeigth() * heigthCoef);
		} else {
			player.getBaseImage().draw(player.getLocation().x * widthCoef,
					player.getLocation().y * heigthCoef,
					player.getWidth() * widthCoef,
					player.getHeigth() * heigthCoef);
		}
	}

	/**
	 * Pause the game and all of it's players
	 */
	private void pause() {
		player1.pause();
		player2.pause();
		gamePaused = true;
	}

	/**
	 * Unpause the game and the players
	 */
	private void unpause() {
		player1.unpause();
		player2.unpause();
		gamePaused = false;
	}

	/**
	 * Resets the animation for the players of the game
	 */
	private void resetPlayersPosition() {
		player1.resetPlayer();
		player2.resetPlayer();
	}

	/**
	 * End the server<br>
	 * Close the sockets, streams and InputFetcher<br>
	 * Close the application
	 */
	private void endServer() {

		P1InputFetcher.stop = true;
		P2InputFetcher.stop = true;

		serverState = ServerState.None;

		try {
			inP2.close();
			inP2 = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			inP1.close();
			inP1 = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			outP2.close();
			outP2 = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			outP1.close();
			outP1 = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			socketP1.close();
		} catch (IOException e) {
			socketP1 = null;
			e.printStackTrace();
		}

		try {
			socketP2.close();
			socketP2 = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			serverSocket.close();
			serverSocket = null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		resetPlayersPosition();
		player1.resetScore();
		player2.resetScore();
		P1InputFetcher = null;
		P2InputFetcher = null;

		System.exit(0);

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {

			AppGameContainer app = new AppGameContainer(new SwordServer(
					"Server"), 600, 400, false);
			app.setAlwaysRender(true);
			app.setTargetFrameRate(120);
			app.setVerbose(true);
			app.setShowFPS(true);
			app.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
